#include <iostream>  // std::cout, std::ostream
#include <sstream>   // std::?stringstream    where ? = i, or o.
#include <fstream>   // std::ifstream
#include <string>    // std::string
#include <vector>    // std::vector


// The 'thing' that will hold the Data
class Cosa {
  private:
    int the_int;
    double the_double;
    std::string the_string;

  public:
      Cosa( int i = 1 , double d = 0.0 , std::string s = ";-(" )
        : the_int(i), the_double(d), the_string(s) { }

      // Why is `out` passed by reference???
      void print_cosa( std::ostream& out ){
          out << "The int: " << the_int << '\n'
              << "The double: " << the_double << '\n'
              << "The string: " << the_string << "\n\n";
          return;
      }
};

int main(){
    // Placing `using` statements here reduces their scope.
    using namespace std;
    // Notice that in the class definition above, the full names
    // (e.g., std::string, std::ostream) have to be used.

    ifstream fin("data.csv");
    vector<Cosa> v;
  
    // We know the structure of the file: Row = int, double, string
    int x;
    double d;
    string s;

    while ( fin >> x >> d >> s ) { 
        Cosa c(x,d,s);
        v.push_back(c);
    }   

    fin.close();

    // Thingys to `cout`
    for ( int i = 0 ; i < v.size(); ++i ) {
        v[i].print_cosa(cout);
    }
    
    // Thingys to file
    ofstream fout("processed_data.txt");
    for ( Cosa c : v ) {                    // <-- Range based loop
        c.print_cosa(fout);
    }
    fout.close();

    return 0;
}
