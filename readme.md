## Lecture: Streams

### The problem: Processing the contents of a `.csv` file

In order to illustrate the use of streams in `C++`, professor Haberland
poses the problem of 

> "... load[_ing_] the data in a file ... " (See class slides: Lec. 9, slide 15--16)

In this repository you can find my particular take on the issue. Just be aware
that I do not quite follow the steps he describes in his algorithm. 

### The code
It is available as a git repository hosted at `bitbucket.org`. My user name
is `rikis-salazar`. Feel free to check out other repositories as well.

There are two branches: *master*, and *fix_this*. 

In **master**, the `.csv` file is "successfully" processed, and the data is
then sent to an output file. While in the intermediate steps `[io]stringstream`
objects are used, on the final step I work only with `[io]fstream` objects.

On the other hand, in **fix_this** I intentionally stop at a "non-working"
stage.  The idea is that you write your own patch (i.e., fix). As it can be
seen from the code, the issue arises when a comma that is being used as a field
separator is read as part of a field. This will hopefully provide an
opportunity for you to find out about ways to _parse_ a string object.
